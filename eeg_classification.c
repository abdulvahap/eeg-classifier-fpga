

/*
 ============================================================================
 Name        : EEGClassificationWithANN.c
 Author      : Sushant Kumar & Abdulvahap Calikoglu
 Version     : Version 1.1
 Copyright   : Copyright 2020 Bremen, Germany. All rights are reserved.
 Description : Artificial neural network implementation in C for EEG Classification.
 ============================================================================

 * eeg_classification.c: EEG Extraction and Classification with Multilayer Perceptron
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialised by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

// Import libraries
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
// XTime header file to calculate
#include "xtime_l.h"
// SDK generated parameters
#include "xparameters.h"
// SD device driver
#include "xsdps.h"
// Import libraries to support Zynq Board
#include "platform.h"
#include "xil_printf.h"
#include "ff.h"
#include "xil_cache.h"
#include "xplatform_info.h"


// Define size of the layers
int neurons_in_input_layer = 178;
int neurons_in_hidden_layer1 = 32;
int neurons_in_hidden_layer2 = 32;
int neurons_in_output_layer = 1;

// Define arrays to store biases
float bias_for_layer1[32];
float bias_for_layer2[32];
float bias_for_layer3[1];

// Define arrays to store data and weights
float test_data_inputs[2300][178];
float test_data_outputs[2300][1];
float weights_for_layer1[178][32];
float weights_for_layer2[32][32];
float weights_for_output_layer[32][1];

// Define function prototypes to read and store data, network parameters and to calculate model output
void mount_sd_card();
void load_bias_for_layer1(TCHAR *file_path);
void load_bias_for_layer2(TCHAR *file_path);
void load_bias_for_layer3(TCHAR *file_path);
void read_input_file(TCHAR * file_path);
void read_label_file(TCHAR * file_path);
void load_weights_for_layer1(TCHAR * file_path);
void load_weights_for_layer_2(TCHAR * file_path);
void load_weights_for_output_layer(TCHAR * file_path);
int * calculate_model_output();
static FATFS fatfs;

int main(void) {

  init_platform();
  mount_sd_card();

  // Read input file, which needs to be tested
  read_input_file("0:/in.csv");

  // Read output labels file, which needs to be compared for accuracy testing
  read_label_file("0:/out.csv");

  // Load weights for hidden layer 1
  load_weights_for_layer1("0:/one.csv");

  // Load weights for hidden layer 2
  load_weights_for_layer_2("0:/two.csv");

  // Load weights for output layer
  load_weights_for_output_layer("0:/three.csv");

  // Load biases for layer 1
  load_bias_for_layer1("0:/bias1.csv");

  // Load biases for layer 2
  load_bias_for_layer2("0:/bias2.csv");

  // Load biases for output layer
  load_bias_for_layer3("0:/bias3.csv");

  // Define variables to count correct and incorrect predictions to calculate model accuracy
  double correct_predictions = 0, incorrect_predictions = 0;

  // Define variables to calculate model execution duration
  XTime start_time, end_time;
  // Get the starting time
  XTime_GetTime( & start_time);

  // Calculate model output that returns int array that has predictions
  int * model_output_pointer;
  model_output_pointer = calculate_model_output();

  // Get the end time
  XTime_GetTime( & end_time);

  // Count correct and incorrect predictions to calculate accuracy
  int l = 0;
  for (l = 0; l < 2300; l++) {
	  // Check if the model outpur match with the actual output data to calculate accuracy
    if ( * (model_output_pointer + l) == test_data_outputs[l][0]) {
    	// If they match, increment correct prediction
      correct_predictions = correct_predictions + 1;
    } else {
    	// If no, increment incorrect prediction
      incorrect_predictions = incorrect_predictions + 1;
    }
  }

  // Calculate and print model accuracy in percentage
  double model_accuracy = (correct_predictions / (correct_predictions + incorrect_predictions)) * 100;
  int samples = 2300;
  printf("Accuracy of model for %d samples = %f percent \n\r", samples, model_accuracy);

  // Zynq counter increases at every two clock cyles
  printf("Output took %llu clock cycles. \n", 2 * (end_time - start_time));
  // Divide counter count by counts per second to convert counter count into seconds
  double elapsed_time = 1.0 * (end_time - start_time) / (COUNTS_PER_SECOND);
  // Print duration needed for model output
  printf("Output took %.2f secs.\n", elapsed_time);

  cleanup_platform();
  return 0;
}

// Define a function to mount SD Card to read files

void mount_sd_card() {

  TCHAR * Path = "0:/";

  FRESULT Res;

  Res = f_mount( & fatfs, Path, 0);

  if (Res != FR_OK) {
    printf("Not OK \n");
  }
  if (Res == FR_OK) {
    printf("SD Card mounted successfully \n");
  }

}


/*********************************************************************************
 =========================READING INPUT FILES======================================
 *********************************************************************************/
// Read input file by passing its path
void read_input_file(TCHAR * file_path) {
  // Define a pointer to store each input sample (row) in the file while tokenizing
  char * line;
  // Define a pointer to store each time point (value) in the file while tokenizing
  char * value;
  // Define variables for matrix indexes
  int data_row1 = 0, data_column1 = 0;

  // Define a pointer to the file object structure
  FIL test_data_inputs_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & test_data_inputs_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);

  // Define number of bytes to read and buffer to store values
  u32 file_size = (2300 * 178 * 15);
  UINT NumBytesRead;
  char DestinationAddress[2300 * 178 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & test_data_inputs_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);

  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize each input sample (line)
    line = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing input samples (file) until we run out of lines
    while (line != NULL) {
      //printf("Line %d : %s \n", data_row1, line);
      // Use comma to tokenize each time point value
      value = strtok_r(line, ",", & line);
      // Keep tokenizing each input sample (line) until we run out of values
      while (value != NULL) {
    	// Convert string into float values to use in calculation
    	test_data_inputs[data_row1][data_column1++] = atof(value);
        // Tokenize next value
    	value = strtok_r(NULL, ",", & line);
      }
      data_column1 = 0;
      data_row1++;
      // Tokenize next line
      line = strtok_r(NULL, "\n", & temp);
    }
    // otherwise print file is not available
  } else {
    printf("Input file is not available in the project folder ! \n");

  }

}

/*********************************************************************************
 =========================READING LABELS FILES=====================================
 *********************************************************************************/

// Read label file by passing its path
void read_label_file(TCHAR * file_path) {
  // Define a pointer to store each label sample (row) in the file while tokenizing
  char * line;
  // Define a pointer to store each label point (value) in the file while tokenizing
  char * value;
  // Define variables for matrix indexes
  int data_row2 = 0, data_column2 = 0;

  // Define a pointer to the file object structure
  FIL test_data_outputs_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & test_data_outputs_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);

  // Define number of bytes to read and buffer to store values
  u32 file_size = (2300 * 15);
  UINT NumBytesRead;
  char DestinationAddress[2300 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & test_data_outputs_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize each label sample (line)
    line = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing label samples (file) until we run out of lines
    while (line != NULL) {
      //printf("Line %d : %s \n", data_row2, line);
      // Use comma to tokenize each time point value
      value = strtok_r(line, ",", & line);
      // Keep tokenizing each label sample (line) until we run out of values
      while (value != NULL) {
    	// Convert string into float values to use in calculation
        test_data_outputs[data_row2][data_column2++] = atof(value);
        // Tokenize next value
        value = strtok_r(NULL, ",", & line);
      }
      data_column2 = 0;
      data_row2++;
      // Tokenize next line
      line = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf("Output label file is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING WEIGHTS of LAYER 1 FILES=========================
 *********************************************************************************/
// Read weights for hidden layer1 by passing its path
void load_weights_for_layer1(TCHAR * file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * line1;
  // Define a pointer to store each value of the respective row while tokenizing
  char * value1;
  // Define variables for matrix indexes
  int weight_row1 = 0, weight_column1 = 0;

  // Define a pointer to the file object structure
  FIL weights_for_layer1_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & weights_for_layer1_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (178 * 32 * 15);
  UINT NumBytesRead;
  char DestinationAddress[178 * 32 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & weights_for_layer1_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    line1 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing (file) until we run out of lines
    while (line1 != NULL) {
      //printf("Line %d : %s \n", weight_row1, line1);
      // Use comma to tokenize each weight presents in each line of the file
      value1 = strtok_r(line1, ",", & line1);
      // Keep tokenizing each weight of each line until we run out of weights
      while (value1 != NULL) {
    	// Convert string into float values to use in calculation
    	weights_for_layer1[weight_row1][weight_column1++] = atof(value1);
    	// Tokenize next value
        value1 = strtok_r(NULL, ",", & line1);
      }
      weight_column1 = 0;
      weight_row1++;
      // Tokenize next line
      line1 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Weights for layer 1 is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING WEIGHTS of LAYER 2 FILES=========================
 *********************************************************************************/
// Read weights for hidden layer2 by passing its path
void load_weights_for_layer_2(TCHAR * file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * line2;
  // Define a pointer to store each value of the respective row while tokenizing
  char * value2;
  // Define variables for matrix indexes
  int weight_row2 = 0, weight_column2 = 0;

  // Define a pointer to the file object structure
  FIL weights_for_layer2_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & weights_for_layer2_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (32 * 32 * 15);
  UINT NumBytesRead;
  char DestinationAddress[32 * 32 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & weights_for_layer2_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    line2 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing each weight of each line until we run out of weights
    while (line2 != NULL) {
      //printf("Line %d : %s \n", weight_row2, line2);
      // Use comma to tokenize each weight presents in each line of the file
      value2 = strtok_r(line2, ",", & line2);
      while (value2 != NULL) {
    	// Convert string into float values to use in calculation
    	weights_for_layer2[weight_row2][weight_column2++] = atof(value2);
    	// Tokenize next value
        value2 = strtok_r(NULL, ",", & line2);
      }
      weight_column2 = 0;
      weight_row2++;
      // Tokenize next line
      line2 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Weights for layer 2 is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING WEIGHTS of OUTPUT LAYER FILES====================
 *********************************************************************************/
// Read weights for output layer by passing its path
void load_weights_for_output_layer(TCHAR * file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * line3;
  // Define a pointer to store each value of the respective row while tokenizing
  char * value3;
  // Define variables for matrix indexes
  int weight_row3 = 0, weight_column3 = 0;

  // Define a pointer to the file object structure
  FIL weights_for_layer3_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & weights_for_layer3_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (64 * 2048);
  UINT NumBytesRead;
  char DestinationAddress[32 * 1 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & weights_for_layer3_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    line3 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing each weight of each line until we run out of weights
    while (line3 != NULL) {
      //printf("Line %d : %s \n", weight_row3, line3);
      // Use comma to tokenize each weight presents in each line of the file
      value3 = strtok_r(line3, ",", & line3);
      while (value3 != NULL) {
    	  // Convert string into float values to use in calculation
    	weights_for_output_layer[weight_row3][weight_column3++] = atof(value3);
    	// Tokenize next value
        value3 = strtok_r(NULL, ",", & line3);
      }
      weight_column3 = 0;
      weight_row3++;
      // Tokenize next line
      line3 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Weights for output layer is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING BIASES of FIRST HIDDEN LAYER====================
 *********************************************************************************/
// Read weights for output layer by passing its path
void load_bias_for_layer1(TCHAR *file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * bias_line1;
  // Define a pointer to store each value of the respective row while tokenizing
  char * bias_value1;
  // Define variables for matrix indexes
  int bias_row1 = 0, bias_column1 = 0;

  // Define a pointer to the file object structure
  FIL bias_for_layer1_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & bias_for_layer1_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (64 * 2048);
  UINT NumBytesRead;
  char DestinationAddress[32 * 1 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & bias_for_layer1_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    bias_line1 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing each weight of each line until we run out of weights
    while (bias_line1 != NULL) {
      //printf("Line in bias1 file %d : %s \n", weight_row3, bias_line1);
      // Use comma to tokenize each weight presents in each line of the file
      bias_value1 = strtok_r(bias_line1, ",", & bias_line1);
      while (bias_value1 != NULL) {
    	  // Convert string into float values to use in calculation
    	  bias_for_layer1[bias_row1] = atof(bias_value1);
    	// Tokenize next value
        bias_value1 = strtok_r(NULL, ",", & bias_line1);
      }
      bias_column1 = 0;
      bias_row1++;
      // Tokenize next line
      bias_line1 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Biases for first hidden layer is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING BIASES of SECOND HIDDEN LAYER====================
 *********************************************************************************/
// Read weights for output layer by passing its path
void load_bias_for_layer2(TCHAR *file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * bias_line1;
  // Define a pointer to store each value of the respective row while tokenizing
  char * bias_value1;
  // Define variables for matrix indexes
  int bias_row2 = 0, bias_column2 = 0;

  // Define a pointer to the file object structure
  FIL bias_for_layer2_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & bias_for_layer2_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (64 * 2048);
  UINT NumBytesRead;
  char DestinationAddress[32 * 1 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & bias_for_layer2_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    bias_line1 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing each weight of each line until we run out of weights
    while (bias_line1 != NULL) {
      //printf("Line in bias2 file %d : %s \n", weight_row3, bias_line1);
      // Use comma to tokenize each weight presents in each line of the file
      bias_value1 = strtok_r(bias_line1, ",", & bias_line1);
      while (bias_value1 != NULL) {
    	  // Convert string into float values to use in calculation
    	  bias_for_layer2[bias_row2] = atof(bias_value1);
    	// Tokenize next value
        bias_value1 = strtok_r(NULL, ",", & bias_line1);
      }
      bias_column2 = 0;
      bias_row2++;
      // Tokenize next line
      bias_line1 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Biases for second hidden layer is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 =========================READING BIASES of LAYER THREE FILES====================
 *********************************************************************************/
// Read weights for output layer by passing its path
void load_bias_for_layer3(TCHAR *file_path) {
  // Define a pointer to store each row in the file while tokenizing
  char * bias_line1;
  // Define a pointer to store each value of the respective row while tokenizing
  char * bias_value1;
  // Define variables for matrix indexes
  int bias_row3 = 0, bias_column3 = 0;

  // Define a pointer to the file object structure
  FIL bias_for_layer3_pointer;
  // Open file and return result to print if it is opened successfully
  FRESULT result = f_open( & bias_for_layer3_pointer, file_path, FA_READ);
  printf("Open operation result : %d \n", result);
  // Define number of bytes to read and buffer to store values
  u32 file_size = (64 * 2048);
  UINT NumBytesRead;
  char DestinationAddress[32 * 1 * 15];
  // Temporary storage buffer to use in string tokenizer to avoid original array modification
  char * temp;
  // Read file and return result to print if it is read successfully
  FRESULT read_result = f_read( & bias_for_layer3_pointer, (void * ) DestinationAddress, file_size, & NumBytesRead);
  printf("Read operation result : %d \n\r", read_result);
  // Check if read_result is ok, if yes start tokenization
  if (read_result == FR_OK) {
	// Use new line to tokenize the entire file (tokenizing row by row)
    bias_line1 = strtok_r(DestinationAddress, "\n", & temp);
    // Keep tokenizing each weight of each line until we run out of weights
    while (bias_line1 != NULL) {
      //printf("Line in bias3 file %d : %s \n", weight_row3, bias_line1);
      // Use comma to tokenize each weight presents in each line of the file
      bias_value1 = strtok_r(bias_line1, ",", & bias_line1);
      while (bias_value1 != NULL) {
    	  // Convert string into float values to use in calculation
    	  bias_for_layer3[bias_row3] = atof(bias_value1);
    	// Tokenize next value
        bias_value1 = strtok_r(NULL, ",", & bias_line1);
      }
      weight_column3 = 0;
      weight_row3++;
      // Tokenize next line
      bias_line1 = strtok_r(NULL, "\n", & temp);
    }
  } else {
    printf(
      "Biases for output layer is not available in the project folder ! \n");
  }
}

/*********************************************************************************
 ==========================CALCULATIONS of MODEL OUTPUT============================
 *********************************************************************************/

int * calculate_model_output() {

  float hidden_layer1[neurons_in_hidden_layer1];
  float hidden_layer2[neurons_in_hidden_layer2];
  int i = 0, j = 0;
  int test_inputs = 0;
  float output[2300];
  static int model_out[2300];

  for (test_inputs = 0; test_inputs < 2300; test_inputs++) {

    // First hidden layer calculations

    float layer_1_product = 0, layer_1_sum = 0;
    for (i = 0; i < neurons_in_hidden_layer1; i++) {
      for (j = 0; j < neurons_in_input_layer; j++) {
        layer_1_product = test_data_inputs[test_inputs][j] *
          weights_for_layer1[j][i];
        layer_1_sum = layer_1_sum + layer_1_product;
      }

      layer_1_sum = layer_1_sum + bias_for_layer1[i];

      //RELU function implementation
      if (layer_1_sum < 0) {
        hidden_layer1[i] = 0;
      } else {
        hidden_layer1[i] = layer_1_sum;
      }

      layer_1_sum = 0;
      layer_1_product = 0;
    }
    i = 0;
    j = 0;

    // Second hidden layer calculations
    float layer_2_product = 0, layer_2_sum = 0;

    for (i = 0; i < neurons_in_hidden_layer1; i++) {
      for (j = 0; j < neurons_in_hidden_layer2; j++) {
        layer_2_product = hidden_layer1[j] * weights_for_layer2[j][i];
        layer_2_sum = layer_2_sum + layer_2_product;
      }
      j = 0;

      layer_2_sum = layer_2_sum + bias_for_layer2[i];

      //RELU function implementation
      if (layer_2_sum < 0) {
        hidden_layer2[i] = 0;
      } else {
        hidden_layer2[i] = layer_2_sum;
      }
      layer_2_sum = 0;
      layer_2_product = 0;
    }
    i = 0;
    j = 0;

    // Output Layer Calculations

    float output_layer_product = 0, output_layer_sum = 0;

    for (i = 0; i < neurons_in_hidden_layer2; i++) {
      output_layer_product = hidden_layer2[i] *
        weights_for_output_layer[i][0];
      output_layer_sum = output_layer_sum + output_layer_product;
    }

    output_layer_sum = output_layer_sum + bias_for_layer3[0];

    // SIGMOID function implementation
    output[test_inputs] =
      (1.0 / (1.0 + exp((float)(-(output_layer_sum)))));

    i = 0;
    output_layer_product = 0;
    output_layer_sum = 0;

    if (output[test_inputs] < 0.5) {
      model_out[test_inputs] = 0;
    } else {
      model_out[test_inputs] = 1;
    }

  }

  return model_out;
}
